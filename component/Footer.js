import React from "react";


const Footer = (props) => {

    return (
        <div className="container-fluid bg-dark ">
            <hr />
            <p className="text-center footer text-light">
                All the latest product drops, limited offers, in-store event info—straight to your inbox.
            </p>
                <div className="container ">
                    <div class="row md-center col-lg-2 ml-12 center-email">
                        <div className = "input-group mb-3 ">
                            <input type="text" className="form-control" placeholder="E-MAIL" aria-label="Recipient's username" aria-describedby="button-addon2" />
                            <div>
                                <button className="btn btn-outline-secondary" type="button" id="button-addon2">-></button>
                            </div>
                         </div>
                    </div>
                </div>
   
                <div className="col-md-12 text-center">
                    <ul className="list-inline ">
                        <li className=" list-inline-item border-bottom ">
                            <img style={{ height: 30, marginLeft: 10 }} src="https://img.icons8.com/ios/50/ffffff/facebook.png" alt="" />
                            <img style={{ height: 30, marginLeft: 10 }} src="https://img.icons8.com/ios/50/ffffff/instagram-new.png" alt="" />
                            <img style={{ height: 30, marginLeft: 10 }} src="https://img.icons8.com/ios/50/ffffff/youtube-play.png" alt="" />
                            <img style={{ height: 30, marginLeft: 10 }} src="https://img.icons8.com/ios-filled/50/ffffff/twitter.png" alt="" />

                        </li>
                    </ul>


                    <h2 className=" text-light"> ABOUT US</h2>

                    <p className="container col-md-8 text-center footer text-light mt-4 mb-4">
                        Fendy Beauty after years of experimenting with the best-of-the-best in
                        beauty—and still seeing a void in the industry for products that performed
                        across all skin types and tones. Starting with Fendy Beauty foundation,
                        launched a makeup line “so that people everywhere would be included,”
            </p>
                </div>
            </div>

    )
}

export default Footer;