import React, { Component } from 'react';
import { Navbar, Nav, Button } from "react-bootstrap";
import Link from "next/link";


class Header extends Component {
    render() {
        return (
            <div className="container-fluid bg-dark">
                <div className=" row">
                    <div className=" col-md-12 text-center  " >
                        <h1 className="col-mt-5 text-light col" ><Link href="/"> FENDY BEATY </Link> </h1>
                    </div>
                    <div className="col-md-12 text-center">
                        <ul className="list-inline">

                            <li className="list-inline-item title "><Link href="/shop" > SHOP </Link></li>
                            <li className="list-inline-item title "><Link href="/product/product" > PRODUCTS </Link></li>
                            <li className="list-inline-item title "><Link href="/order" > YOUR ORDER </Link></li>
                            <li className="list-inline-item title "><Link href="/review/review" > REVIEW </Link></li>

                        </ul>

                    </div>
                    <div className="col-md-12 text-right">
                        <ul className="list-inline ">
                            <li className=" list-inline-item  ">
                                <button type="button" className="btn" >
                                <Link href="/login">
                                    <img style={{ height: 30, marginLeft: 10 }} src="https://img.icons8.com/ios/50/ffffff/search-for-love.png" alt="Login" />
                                </Link>
                                 Login
                                </button>

                                <button type="button" className="btn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                    <Link href="/register">
                                    <img style={{ height: 30, marginLeft: 10 }} src="https://img.icons8.com/ios/50/ffffff/user-female-circle.png" alt="Register" />
                                    </Link>
                                    Regiter
                                </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                                <button type="button" className="btn" >
                                    <img style={{ height: 30, marginLeft: 10 }} src="https://img.icons8.com/ios/50/ffffff/shopping-bag.png" alt="" />
                                </button>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        );
    }
}

export default Header;