import React , { useState , useEffect } from 'react';
import Layout from "../../component/Layout";
import { getAllPocduct } from "../../component/api";
import ProductTable from "../product/ProductTable";

export default function Product() {
    const [products, setProducts] = useState([]);

    useEffect(() => {
      fetchAllProducts();
    }, []);
  
    const fetchAllProducts = async () => {
      await getAllPocduct().then((res) => {
        if (res.status === "success") {
          setProducts(res.data);
        }
      });
    };
  
  
    return (
      <Layout >
       
        <div className="container">
          <div className="menubar">
          <h1 style={{textAlign:"center"}}>Product</h1>
            <div className="menubar-container">
            </div>
          </div>
          <ProductTable products={products} />
        </div>
      </Layout>
    );
  }