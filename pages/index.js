import React, { Component } from 'react';
import Link from "next/link";
import Layout from "../component/Layout";
import axios from "axios";
import Homeslide from "../component/Homeslide/Homeslide";
import Catagory from "../pages/product/Catagory";
import History from "../helper/History";

class Index extends Component {

    static async getInitialProps() {
        const res = await axios.get("http://localhost:3002/products")
        return { products: res.data }
    }
    renderProduct = products => {
        return (
            products.map(product => {
                return (
                    <div key={product.id} className="col-6 row-3">
                        <img src={product.thumbnail} className="img-fluid" />
                        {product.productName}
                    </div>

                )
            })

        );
    }
    render() {
        return (
            <Layout  >
                
               <Homeslide />
               <Catagory />
                

                    <div className="container">
                        <div className="row">
                            {this.renderProduct(this.props.products)}
                        </div>
                    </div>
            </Layout>
        )
    }
}

export default Index;