import React from 'react'
import Layout from "../component/Layout";
import { UserRegis } from "../component/api";
import { useState } from 'react';
import {useRouter} from 'next/router';
export default function register(props) {
    const router = useRouter();

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [name, setName] = useState("");
    const [age, setAge] = useState(0);
    const [salary, setSalary] = useState(0);
  


    const save = async (e) => {
        e.preventDefault()
        let user = {
            username: username,
            password: password,
            name: name,
            age: age,
            salary: salary,
        };
        let result = await UserRegis(user)
        if(result.status==='sucecss'){
            alert('สมัครเรียบร้อยแล้ว');
        //console.log(result)   
        router.push("/login");
        }
    };

    return (
        <div>
           
        <Layout >
          
        <div className="container">
            <div style={{ textAlign: 'center' }}> 
                <img   img src="/static/images/product/bg22.jpg" width="200"></img>
                <h2>CREATE AN ACCOUNT</h2>
            </div>
            <form onSubmit={save}>
                <div className="container">
                    <div className="row">
                        <div className="col-sm">
                            <div class="form-group mr-sm-2">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" 
                                value={username} onChange={(e) => setUsername(e.target.value)} 
                                id="username" />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm">
                            <div class="form-group mr-sm-2">
                                <label for="password">Password</label>
                                <input type="password" class="form-control mr-sm-2" 
                                value={password} onChange={(e) => setPassword(e.target.value)}
                                 id="password" />
                            </div>
                        </div>
                        <div className="col-sm">
                            <div class="form-group mr-sm-2">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" 
                                value={name} onChange={(e) => setName(e.target.value)} 
                                id="name" />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm">
                            <div class="form-group mr-sm-2">
                                <label for="age">Age</label>
                                <input type="number" class="form-control" 
                                value={age} onChange={(e) => setAge(e.target.value)} 
                                id="age" />
                            </div>
                        </div>
                        <div className="col-sm">
                            <div class="form-group mr-sm-2">
                                <label for="salary">Salary</label>
                                <input type="text" class="form-control" 
                                value={salary} onChange={(e) => setSalary(e.target.value)} 
                                id="salary" />
                            </div>
                        </div>
                    

                    </div>

                    <button type="submit" class="btn btn-outline-dark center-email" >Create An Account</button>
                </div>


            </form>
        </div>
        </Layout>
        </div>
         
    )
}
