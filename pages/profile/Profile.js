import React,{useState,useEffect} from 'react'
import Layout from "../../component/Layout";
import Link from 'next/link';
import { getUserById} from '../../component/api';



export default function Profile() {

    const [userProfile,setUserProfile ] = useState([]);

    useEffect(() => {
        fetchUser();
    }, []);

    const fetchUser = async () =>{
        await getUserById(localStorage.getItem("user_id")).then((res)=>
        {
            if (res.status === "success") {
                setUserProfile(res.data);
              }
        })
    }

    return (
        <div>
            <Layout>
            <div className="profile-area d-flex align-items-center justify-content-center">
        <div className="container">
          <h1>Profile </h1>
          {
            <div>
              <div className="row">
                <div className="col-12 d-flex justify-content-center">
                  <h2>{userProfile.name}</h2>
               
                </div>
              </div>
              <div className="row">
                <div className="col-12">
                  <hr />
                    <h4>Age : {userProfile.age}</h4> 
                    <h4>Salary : {userProfile.salary}</h4> 
                  
                </div>
              </div>
              <div className="btn bg-copm ">
                 
                    <Link  href={'editprofile'} className="btn btn-outline-dark border">Edit Profile</Link>
                  
                </div>  
            </div>
          }
        </div>
      </div>
            </Layout>
            
        </div>
    )
}
