import React,{ useState ,useEffect } from 'react';
import { getUserById, editUserById } from '../../component/api';
import { useRouter } from 'next/router';
import Layout from '../../component/Layout';


export default function EditProfile(props) {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [name, setName] = useState("");
    const [age, setAge] = useState(0);
    const [salary, setSalary] = useState(0);
  
    const router = useRouter();
    
    const handleEditProfile = async (e) =>{
        e.preventDefault();
        let dataEdit ={
            username: username,
            password: password,
            name: name,
            age: age,
            salary: salary
        }
        await editUserById(localStorage.getItem("user_id"), dataEdit).then((res) =>{
            if (res.status === "success"){
                router.push("/profile/profile");
            }
        })
    }

    const fetchUser = async () =>
    {
        await getUserById(localStorage.getItem("user_id")).then((res)=>{
            if(res.status ==="success"){
                setUsername(res.data.username);
                setPassword(res.data.password);
                setName(res.data.name);
                setAge(res.data.age);
                setSalary(res.data.salary);
            }
        })
    }
    useEffect(()=>{
        fetchUser();
    },[]);

    return (
        <Layout>
        <div className="container">
            <div className="row">
            <div className="content-center">
              <h1>EDIT PROFILE</h1>
            </div>
            <div className="content-center text-left">
              <form onSubmit={handleEditProfile}>
              <div className="container">
                    <div className="row">
                        <div className="col-sm">
                            <div class="form-group mr-sm-2">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" 
                                value={username} onChange={(e) => setUsername(e.target.value)} 
                                id="username" />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm">
                            <div class="form-group mr-sm-2">
                                <label for="password">Password</label>
                                <input type="password" class="form-control mr-sm-2" 
                                value={password} onChange={(e) => setPassword(e.target.value)}
                                 id="password" />
                            </div>
                        </div>
                        <div className="col-sm">
                            <div class="form-group mr-sm-2">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" 
                                value={name} onChange={(e) => setName(e.target.value)} 
                                id="name" />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm">
                            <div class="form-group mr-sm-2">
                                <label for="age">Age</label>
                                <input type="number" class="form-control" 
                                value={age} onChange={(e) => setAge(e.target.value)} 
                                id="age" />
                            </div>
                        </div>
                        <div className="col-sm">
                            <div class="form-group mr-sm-2">
                                <label for="salary">Salary</label>
                                <input type="text" class="form-control" 
                                value={salary} onChange={(e) => setSalary(e.target.value)} 
                                id="salary" />
                            </div>
                        </div>
                    

                    </div>
                    <button type="submit" class="btn btn-outline-dark center-email " >Edit your profile</button>
                    
                </div>
                </form>
            </div>
            </div>
        </div>
        </Layout>
    )
}
