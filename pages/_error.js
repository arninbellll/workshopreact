import React, { Component } from "react";
import Layout from "../component/Layout";

class Error extends Component {
  static getInitialProps({ res, err }) {
    const statusCode = res ? res.statusCode : err ? err.statusCode : null;
    return { statusCode };
  }

  render() {
    return (
      <Layout>
        <div className="container">
          <div className="text-center">
            <h3>
            {this.props.statusCode
              ? "An Error " + this.props.statusCode + " occured on server"
              : "An error occured on client"}
              </h3>
          </div>
        </div>
      </Layout>
    );
  }
}

export default Error;
